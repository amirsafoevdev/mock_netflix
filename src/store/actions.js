import { GET_DATA } from './types'

export const getData = ()=> {
    return dispatch => {
    fetch('titles.json')
        .then(response => response.json())
        .then(data => {
            dispatch({
            type: GET_DATA,
            payload: data
            })
        })
    }
}
