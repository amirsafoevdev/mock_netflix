import * as actionTypes from './types'

const initialState = 
    {
        mylist : [],
        recommendations : []
    }

const reducer = (state = initialState, action) => {
switch(action.type){
    case actionTypes.GET_DATA:
        return state = action.payload
    case actionTypes.ADD_TO_LIST:
        let recList = state.recommendations
        let found = state.mylist.find(function(movie) {return movie.id === action.payload.id})
        let filterRec = recList.filter(movie => movie.id !== action.payload.id)
        if (!found) {
            return {...state, mylist: [...state.mylist, action.payload], recommendations: filterRec}
        }else{
            return {...state, mylist: [...state.mylist]}
        }

    case actionTypes.REMOVE_FROM_LIST:
        let myList = state.mylist
        let resultArrMyList = myList.filter(movie => movie.id !== action.payload.id)  
        
        return {...state, mylist: resultArrMyList, recommendations: [...state.recommendations, action.payload]}        
    default:
        return state;
    }
}

export default reducer