import React from 'react';
import './App.css';
import TileContainer from './components/TileContainer'
import logo from './netflix.jpeg'

function App() {
  return (
    <div>
      <img className="myLogo"
        src={logo}
        alt="logo"
      />
      <TileContainer/>
    </div>
  );
}

export default App;
