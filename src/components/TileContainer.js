import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Container, Row, Col  } from 'react-bootstrap'
import * as actionTypes from '../store/types'

import { getData } from '../store/actions'

import Tile from './Tile'



const TileContainer = () =>{
    const recs = useSelector(state => state.recommendations)
    const mylist = useSelector(state => state.mylist)
   
    const dispatch = useDispatch(); 
    useEffect(() => {
        dispatch(getData())
    },[dispatch])

    function onAdd(movie){
        dispatch({type: actionTypes.ADD_TO_LIST, payload: movie})
    }

    function onRemove(movie){
        dispatch({type: actionTypes.REMOVE_FROM_LIST, payload: movie})
    }
 
    return (
            <Container className="container">
                <h4>My list</h4>
                <Row>
                    {mylist.map(movie => (
                        <Tile key={movie.id}
                        movie={movie}
                        title={movie.title}
                        img={movie.img}
                        onRemove={onRemove}
                        />
                    ))}
                </Row>
                <hr/>
                <h4>Recommendations</h4>
                <Row>
                    {recs.map(movie => (
                        <Tile key={movie.id}
                        movie={movie}
                        title={movie.title}
                        img={movie.img}
                        onAdd={onAdd}
                        />
                    ))}
                </Row>
                <hr/>
                <h4>
                    Titles from My List
                </h4>
                <Row>
                    {mylist.map(movie => 
                    <Col xs lg="2" key={movie.title}>
                        <p>
                            {movie.title}
                        </p>
                    </Col>
                     )}
                </Row>
            </Container>
    )
}

export default TileContainer