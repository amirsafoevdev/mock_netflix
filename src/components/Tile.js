import React, { useState } from 'react'
import { Col, Button } from 'react-bootstrap'

const Tile = (props) => {
    const[status, setStatus] = useState(false)
    let {img, title, movie }= props
    
    if(props.onRemove){
        return(
            <Col xs lg="2" onMouseLeave={()=> setStatus(false)}>
            <h6>
                {title}
            </h6>
            <img src={img} alt={img} onMouseOver={()=> setStatus(true)}/>
            {status ? 
                <Button variant="danger"  active
                onClick={() => { props.onRemove(movie); setStatus(false)}}
                >
                    Remove from list
                </Button> : null}
            </Col>
        )
        
    }else{
        return(
            <Col xs lg="2" onMouseLeave={()=> setStatus(false)}>
            <h6>
                {title}
            </h6>
            <img src={img} alt={img} onMouseOver={()=> setStatus(true)}/>
            {status ? 
                <Button variant="info"  active
                onClick={() => { props.onAdd(movie); setStatus(false)}}
                >
                    Add to list
                </Button> : null}
            </Col>
        )
    }
    
}
export default Tile